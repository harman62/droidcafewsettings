// Name: Harman Kaur
//Student Id: C0744086


package com.example.droidcafewsettings;

import android.content.Intent;
import android.os.Build;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.widget.AppCompatImageView;
import android.view.Menu;
import android.widget.Button;

import static org.hamcrest.CoreMatchers.equalTo;
import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertThat;
import static org.robolectric.Shadows.shadowOf;

import org.checkerframework.dataflow.qual.TerminatesExecution;
import org.junit.Before;
import org.junit.Test;

import org.junit.runner.RunWith;
import org.robolectric.Robolectric;
import org.robolectric.RobolectricTestRunner;
import org.robolectric.Shadows;
import org.robolectric.annotation.Config;
import org.robolectric.shadow.api.Shadow;
import org.robolectric.shadows.ShadowActivity;
import org.robolectric.shadows.ShadowIntent;
import org.robolectric.shadows.ShadowToast;

import android.support.v7.widget.Toolbar;
import android.widget.EditText;
import android.widget.ImageView;

@RunWith(RobolectricTestRunner.class)

public class MainActivityTest {

    private MainActivity activity;
    OrderActivity orderActivity;


    @Before
    public void setUp() throws Exception {
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
    }

    @Test
    public void activityIsNotNull() throws Exception {
        assertNotNull(activity);
    }

    //TC1:   Ordering a Frozen Yogurt
    @Test
    public void testToastOnItemSelection() {
        // 1. Find the image to be clicked
        ImageView imgView = (ImageView) activity.findViewById(R.id.froyo);
        // 2. Click on the image
        imgView.performClick();
        // 3. fetch the toast messgae displayed on click of the image
        String actualOutput = ShadowToast.getTextOfLatestToast().toString();
        // 4. Check if the expected Output(toast message) is equal to the expected output(toast message)
        assertThat(actualOutput, equalTo("You ordered a FroYo."));

    }

    @Config(sdk = Build.VERSION_CODES.O_MR1)

    //TC2:  Cart page remembers user info
    @Test
    public void testAppRemembersUserInformation() throws InterruptedException {

        //1. Find the button that redirects to the next activity
        FloatingActionButton floatingButton = (FloatingActionButton) activity.findViewById(R.id.fab);

        //2. Click teh button
        floatingButton.performClick();
        Thread.sleep(5000);

        //3. Check if it opens the new activity
        orderActivity = Robolectric.buildActivity(OrderActivity.class).create().resume().get();
        ShadowActivity shadowActivity = shadowOf(activity);
        Intent intent = shadowActivity.getNextStartedActivity();
        ShadowIntent shadowIntent = shadowOf(intent);
        assertThat(shadowIntent.getIntentClass().getName(), equalTo(OrderActivity.class.getName()));

        //4. Enter the user information in the required fields
        EditText edtName = (EditText) orderActivity.findViewById(R.id.name_text);
        EditText edtAddress = (EditText) orderActivity.findViewById(R.id.address_text);
        EditText edtPhone = (EditText) orderActivity.findViewById(R.id.phone_text);

        edtName.setText("Harman");
        edtAddress.setText("265 Yorkland Blvd");
        edtPhone.setText("4379880000");

        //5. Find the button that saves the user information
        Button saveBtn = (Button) orderActivity.findViewById(R.id.saveButton);

        //6. Click the button
        saveBtn.performClick();
        Thread.sleep(5000);

        //7. Go back to the main activity
        activity = Robolectric.buildActivity(MainActivity.class)
                .create()
                .resume()
                .get();
        Thread.sleep(5000);

        //8. Open the next activity again
        floatingButton.performClick();
        Thread.sleep(5000);

        //9. CHeck if the expected output matches the actual output
        String name = edtName.getText().toString();
        assertThat(name, equalTo("Harman"));
        String address = edtAddress.getText().toString();
        assertThat(address, equalTo("265 Yorkland Blvd"));
        String phone = edtPhone.getText().toString();
        assertThat(phone, equalTo("4379880000"));
    }

}
